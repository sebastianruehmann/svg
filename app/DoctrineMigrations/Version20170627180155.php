<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20170627180155 extends AbstractMigration
{
    private $groups = array(
        'ROLE_SUPPORT' => 'Support',
        'ROLE_SERVICE' => 'Servicekraft',
        'ROLE_CLERK' => 'Sachbearbeiter',
        'ROLE_EVENT_CLERK' => 'Eventpflege',
        'ROLE_ADMIN' => 'Administrator'
    );

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
      foreach ($this->groups AS $key => $group) {
        $this->addSql('INSERT INTO `group` (name, description) VALUES ("'. $key .'", "'. $group .'")');
      }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
      foreach (array_keys($this->groups) AS $group) {
        $this->addSql('DELETE FROM `group` WHERE name="'. $group .'"');
      }
      $this->addSql('ALTER TABLE `group` AUTO_INCREMENT = 1');
    }
}
