<?php
namespace AppBundle\Service;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class DocumentUploader
{
    private $targetDir;
    private $sanitizer;

    public function __construct($targetDir, Sanitizer $sanitizer)
    {
        $this->targetDir = $targetDir;
        $this->sanitizer = $sanitizer;
    }

    public function upload(UploadedFile $file)
    {
        $sanitizedFilename = $this->sanitizer->filename(pathinfo($file->getClientOriginalName())['filename']);
        $filePath = $sanitizedFilename . '.' . $file->getClientOriginalExtension();

        $file->move(
                $this->targetDir, $filePath
        );

        return $filePath;
    }

    public function getTargetDir()
    {
        return $this->targetDir;
    }
}
