<?php
namespace AppBundle\Service\Calendar;

class Day
{

    /* --------------------------------------------------------------
     * VARIABLES
     * ------------------------------------------------------------ */

    protected $number;
    protected $month;
    protected $year;

    protected $events = array();

    protected $today = FALSE;
    protected $blank = FALSE;
    protected $past = FALSE;

    /* --------------------------------------------------------------
     * GENERIC METHODS
     * ------------------------------------------------------------ */

    public function __construct($day = FALSE, $month = FALSE, $year = FALSE, $events = array(), $today = FALSE, $past = FALSE)
    {
        if ($day == FALSE)
        {
            // Blank cell
            $this->blank = TRUE;
        }
        else
        {
            $this->number = $day;
            $this->month = $month;
            $this->year = $year;
            $this->today = $today;
            $this->past = $past;

            if ($events)
            {
                foreach ($events as $event)
                {
                    $this->events[] = new Event($event);
                }
            }
        }
    }

    /* --------------------------------------------------------------
     * FORMATTING
     * ------------------------------------------------------------ */

    /**
     * Is it today? If so, return a string (useful for today classes)
     */
    public function today_class($class = FALSE)
    {
        return ($this->today) ? ($class ?: 'today') : '';
    }

    public function past_class($class = FALSE)
    {
        return ($this->past) ? ($class ?: 'past') : '';
    }

    /* --------------------------------------------------------------
     * GETTERS
     * ------------------------------------------------------------ */

    public function number() { return $this->number; }
    public function month() { return $this->month; }
    public function year() { return $this->year; }
    public function events() { return $this->events; }
    public function eventCount() { return count($this->events); }
    public function today() { return $this->today; }
    public function blank() { return $this->blank; }
}
