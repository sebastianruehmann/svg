<?php
namespace AppBundle\Entity;

class Event
{
    private $id;
    private $name;
    private $active;
    private $approved;
    private $important;
    private $popup;
    private $specials;
    private $description;
    private $regularPrice;
    private $technicalDescription;

    private $sender;

    private $creator;
    private $creationDate;

    private $documents;
    private $comments;
    private $location;

    private $action;
    private $altAction;

    private $secondAction;

    private $inspector;
    private $inspectionDate;

    private $technicalAction;

    private $secondTechnicalAction;

    private $technicalInspector;
    private $technicalInspectionDate;

    private $secondTechnicalInspector;
    private $secondTechnicalInspectionDate;

    private $startTime;
    private $endTime;

    private $startDate;
    private $endDate;
    private $interval;

    private $type; // normal, info, info Start/Stop, recurring

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->comments = new \Doctrine\Common\Collections\ArrayCollection();
        $this->documents = new \Doctrine\Common\Collections\ArrayCollection();
        $this->interval = "P0D";
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Event
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Event
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set important
     *
     * @param boolean $important
     *
     * @return Event
     */
    public function setImportant($important)
    {
        $this->important = $important;

        return $this;
    }

    /**
     * Get important
     *
     * @return boolean
     */
    public function getImportant()
    {
        return $this->important;
    }

    /**
     * Set popup
     *
     * @param boolean $popup
     *
     * @return Event
     */
    public function setPopup($popup)
    {
        $this->popup = $popup;

        return $this;
    }

    /**
     * Get popup
     *
     * @return boolean
     */
    public function getPopup()
    {
        return $this->popup;
    }

    /**
     * Set specials
     *
     * @param string $specials
     *
     * @return Event
     */
    public function setSpecials($specials)
    {
        $this->specials = $specials;

        return $this;
    }

    /**
     * Get specials
     *
     * @return string
     */
    public function getSpecials()
    {
        return $this->specials;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Event
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set regularPrice
     *
     * @param boolean $regularPrice
     *
     * @return Event
     */
    public function setRegularPrice($regularPrice)
    {
        $this->regularPrice = $regularPrice;

        return $this;
    }

    /**
     * Get regularPrice
     *
     * @return boolean
     */
    public function getRegularPrice()
    {
        return $this->regularPrice;
    }

    /**
     * Set technicalDescription
     *
     * @param string $technicalDescription
     *
     * @return Event
     */
    public function setTechnicalDescription($technicalDescription)
    {
        $this->technicalDescription = $technicalDescription;

        return $this;
    }

    /**
     * Get technicalDescription
     *
     * @return string
     */
    public function getTechnicalDescription()
    {
        return $this->technicalDescription;
    }

    /**
     * Set inspectionDate
     *
     * @param \DateTime $inspectionDate
     *
     * @return Event
     */
    public function setInspectionDate($inspectionDate)
    {
        $this->inspectionDate = $inspectionDate;

        return $this;
    }

    /**
     * Get inspectionDate
     *
     * @return \DateTime
     */
    public function getInspectionDate()
    {
        return $this->inspectionDate;
    }

    /**
     * Set technicalInspectionDate
     *
     * @param \DateTime $technicalInspectionDate
     *
     * @return Event
     */
    public function setTechnicalInspectionDate($technicalInspectionDate)
    {
        $this->technicalInspectionDate = $technicalInspectionDate;

        return $this;
    }

    /**
     * Get technicalInspectionDate
     *
     * @return \DateTime
     */
    public function getTechnicalInspectionDate()
    {
        return $this->technicalInspectionDate;
    }

    /**
     * Set interval
     *
     * @param DateInterval $interval
     *
     * @return Event
     */
    public function setInterval($interval)
    {
        $format = $interval->format("P%yY%mM%dD%hH%iM%sS");
        $format = str_replace(["M0S", "H0M", "D0H", "M0D", "Y0M", "P0Y"], ["M", "H", "D", "M", "Y0M", "P"], $format);

        $this->interval = $format;

        return $this;
    }

    /**
     * Get interval
     *
     * @return string
     */
    public function getInterval()
    {
        return new \DateInterval($this->interval);
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return Event
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     *
     * @return Event
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     *
     * @return Event
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set startTime
     *
     * @param \DateTime $startTime
     *
     * @return Event
     */
    public function setStartTime($startTime)
    {
        $this->startTime = $startTime;

        return $this;
    }

    /**
     * Get startTime
     *
     * @return \DateTime
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * Set endTime
     *
     * @param \DateTime $endTime
     *
     * @return Event
     */
    public function setEndTime($endTime)
    {
        $this->endTime = $endTime;

        return $this;
    }

    /**
     * Get endTime
     *
     * @return \DateTime
     */
    public function getEndTime()
    {
        return $this->endTime;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Event
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set sender
     *
     * @param \AppBundle\Entity\Sender $sender
     *
     * @return Event
     */
    public function setSender(\AppBundle\Entity\Sender $sender = null)
    {
        $this->sender = $sender;

        return $this;
    }

    /**
     * Get sender
     *
     * @return \AppBundle\Entity\Sender
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * Set creator
     *
     * @param \AppBundle\Entity\User $creator
     *
     * @return Event
     */
    public function setCreator(\AppBundle\Entity\User $creator = null)
    {
        $this->creator = $creator;

        return $this;
    }

    /**
     * Get creator
     *
     * @return \AppBundle\Entity\User
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * Set inspector
     *
     * @param \AppBundle\Entity\User $inspector
     *
     * @return Event
     */
    public function setInspector(\AppBundle\Entity\User $inspector = null)
    {
        $this->inspector = $inspector;

        return $this;
    }

    /**
     * Get inspector
     *
     * @return \AppBundle\Entity\User
     */
    public function getInspector()
    {
        return $this->inspector;
    }

    /**
     * Set technicalInspector
     *
     * @param \AppBundle\Entity\User $technicalInspector
     *
     * @return Event
     */
    public function setTechnicalInspector(\AppBundle\Entity\User $technicalInspector = null)
    {
        $this->technicalInspector = $technicalInspector;

        return $this;
    }

    /**
     * Get technicalInspector
     *
     * @return \AppBundle\Entity\User
     */
    public function getTechnicalInspector()
    {
        return $this->technicalInspector;
    }

    /**
     * Set location
     *
     * @param \AppBundle\Entity\Location $location
     *
     * @return Event
     */
    public function setLocation(\AppBundle\Entity\Location $location = null)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return \AppBundle\Entity\Location
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set action
     *
     * @param \AppBundle\Entity\Action $action
     *
     * @return Event
     */
    public function setAction(\AppBundle\Entity\Action $action = null)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action
     *
     * @return \AppBundle\Entity\Action
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set technicalAction
     *
     * @param \AppBundle\Entity\Action $technicalAction
     *
     * @return Event
     */
    public function setTechnicalAction(\AppBundle\Entity\Action $technicalAction = null)
    {
        $this->technicalAction = $technicalAction;

        return $this;
    }

    /**
     * Get technicalAction
     *
     * @return \AppBundle\Entity\Action
     */
    public function getTechnicalAction()
    {
        return $this->technicalAction;
    }

    /**
     * Add comment
     *
     * @param \AppBundle\Entity\Comment $comment
     *
     * @return Event
     */
    public function addComment(\AppBundle\Entity\Comment $comment)
    {
        $this->comments[] = $comment;

        return $this;
    }

    /**
     * Remove comment
     *
     * @param \AppBundle\Entity\Comment $comment
     */
    public function removeComment(\AppBundle\Entity\Comment $comment)
    {
        $this->comments->removeElement($comment);
    }

    /**
     * Get comments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set approved
     *
     * @param boolean $approved
     *
     * @return Event
     */
    public function setApproved($approved)
    {
        $this->approved = $approved;

        return $this;
    }

    /**
     * Get approved
     *
     * @return boolean
     */
    public function getApproved()
    {
        return $this->approved;
    }

    /**
     * Add document
     *
     * @param \AppBundle\Entity\Document $document
     *
     * @return Event
     */
    public function addDocument(\AppBundle\Entity\Document $document)
    {
        $this->documents[] = $document;

        return $this;
    }

    /**
     * Remove document
     *
     * @param \AppBundle\Entity\Document $document
     */
    public function removeDocument(\AppBundle\Entity\Document $document)
    {
        $this->documents->removeElement($document);
    }

    /**
     * Get documents
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDocuments()
    {
        return $this->documents;
    }
    /**
     * @var \DateTime
     */
    private $secondInspectionDate;

    /**
     * @var \AppBundle\Entity\User
     */
    private $secondInspector;


    /**
     * Set secondAction
     *
     * @param boolean $secondAction
     *
     * @return Event
     */
    public function setSecondAction($secondAction)
    {
        $this->secondAction = $secondAction;

        return $this;
    }

    /**
     * Get secondAction
     *
     * @return boolean
     */
    public function getSecondAction()
    {
        return $this->secondAction;
    }

    /**
     * Set secondTechnicalAction
     *
     * @param boolean $secondTechnicalAction
     *
     * @return Event
     */
    public function setSecondTechnicalAction($secondTechnicalAction)
    {
        $this->secondTechnicalAction = $secondTechnicalAction;

        return $this;
    }

    /**
     * Get secondTechnicalAction
     *
     * @return boolean
     */
    public function getSecondTechnicalAction()
    {
        return $this->secondTechnicalAction;
    }

    /**
     * Set secondInspectionDate
     *
     * @param \DateTime $secondInspectionDate
     *
     * @return Event
     */
    public function setSecondInspectionDate($secondInspectionDate)
    {
        $this->secondInspectionDate = $secondInspectionDate;

        return $this;
    }

    /**
     * Get secondInspectionDate
     *
     * @return \DateTime
     */
    public function getSecondInspectionDate()
    {
        return $this->secondInspectionDate;
    }

    /**
     * Set secondTechnicalInspectionDate
     *
     * @param \DateTime $secondTechnicalInspectionDate
     *
     * @return Event
     */
    public function setSecondTechnicalInspectionDate($secondTechnicalInspectionDate)
    {
        $this->secondTechnicalInspectionDate = $secondTechnicalInspectionDate;

        return $this;
    }

    /**
     * Get secondTechnicalInspectionDate
     *
     * @return \DateTime
     */
    public function getSecondTechnicalInspectionDate()
    {
        return $this->secondTechnicalInspectionDate;
    }

    /**
     * Set secondInspector
     *
     * @param \AppBundle\Entity\User $secondInspector
     *
     * @return Event
     */
    public function setSecondInspector(\AppBundle\Entity\User $secondInspector = null)
    {
        $this->secondInspector = $secondInspector;

        return $this;
    }

    /**
     * Get secondInspector
     *
     * @return \AppBundle\Entity\User
     */
    public function getSecondInspector()
    {
        return $this->secondInspector;
    }

    /**
     * Set secondTechnicalInspector
     *
     * @param \AppBundle\Entity\User $secondTechnicalInspector
     *
     * @return Event
     */
    public function setSecondTechnicalInspector(\AppBundle\Entity\User $secondTechnicalInspector = null)
    {
        $this->secondTechnicalInspector = $secondTechnicalInspector;

        return $this;
    }

    /**
     * Get secondTechnicalInspector
     *
     * @return \AppBundle\Entity\User
     */
    public function getSecondTechnicalInspector()
    {
        return $this->secondTechnicalInspector;
    }

    /**
     * Set altAction
     *
     * @param string $altAction
     *
     * @return Event
     */
    public function setAltAction($altAction)
    {
        $this->altAction = $altAction;

        return $this;
    }

    /**
     * Get altAction
     *
     * @return string
     */
    public function getAltAction()
    {
        return $this->altAction;
    }
}
