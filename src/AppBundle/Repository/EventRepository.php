<?php
namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class EventRepository extends EntityRepository
{
    public function findAllFiltered($location, $state) {
        $qb = $this->createQueryBuilder('e');
        if($location != '') {
          $qb->andWhere('e.location = :location')
              ->setParameter('location', $location);
        }
        if($state != '') {
            if($state) {
                $qb->andWhere($qb->expr()->isNotNull('e.approved'));
            } else {
                $qb->andWhere($qb->expr()->isNull('e.approved'));
            }
        }
        return $qb
            ->orderBy('e.startDate', 'DESC')
            ->getQuery()
            ->execute();
    }
    public function findAllActive() {
        $qb = $this->createQueryBuilder('e');
        return $qb
            ->where('e.active = 1')
            ->getQuery()
            ->execute();
    }
    public function findLikeName($name) {
        $qb = $this->createQueryBuilder('e');
        return $qb
            ->select('DISTINCT e.name')
            ->where('LOWER(e.name) LIKE LOWER(:name)')
            ->setParameter('name', $name . '%')
            ->getQuery()
            ->execute();
    }
    public function findAllTechnical() {
        $qb = $this->createQueryBuilder('e');
        return $qb
            ->where('e.active = 1')
            ->andWhere($qb->expr()->isNotNull('e.technicalAction'))
            ->getQuery()
            ->execute();
    }
    public function findAllDaily(\Datetime $date, $technical = false)
    {
        $formattedDate = $date->format('Y-m-d');
        $queryBuilder = $this->createQueryBuilder('e');

        if($technical) {
            $queryBuilder = $queryBuilder->where($queryBuilder->expr()->isNotNull('e.technicalAction'));
        }

        $events = $queryBuilder
            ->where('e.active = 1')
            ->andWhere(':date BETWEEN e.startDate AND e.endDate')
            ->setParameter('date', $formattedDate )
            ->getQuery()
            ->execute();

        foreach($events AS $key => $event) {
            if($event->getType() == "recurring") {
                $inInterval = false;
                $currentDate = clone $event->getStartDate();
                $currentDate->sub($event->getInterval());
                while($currentDate->add($event->getInterval()) <= $event->getEndDate()) {
                    if($date == $currentDate) {
                        $inInterval = true;
                    }
                }
                if(!$inInterval) {
                    array_splice($events, $key, 1);
                }
            }
        }
        return $events;
    }
    public function needsPopup() {
      $now = new \DateTime();
      $now = $now->format('Y-m-d H:i:s');

      $qb = $this->createQueryBuilder('e');
      return $qb
          ->where('e.active = 1')
          ->andWhere($qb->expr()->isNull('e.inspectionDate'))
          ->andWhere($qb->expr()->between(
            $qb->expr()->concat('e.startDate',
              $qb->expr()->concat(
                $qb->expr()->literal(' '), 'e.startTime')
            ),
            ':now',
            ':reminderTime'
          ))
          ->setParameter('reminderTime', new \DateTime('+5 minutes'), \Doctrine\DBAL\Types\Type::DATETIME)
          ->setParameter('now', $now)
          ->getQuery()
          ->execute();
    }
    public function needsNotActivePopup() {
      $now = new \DateTime();
      $now = $now->format('Y-m-d H:i:s');

      $qb = $this->createQueryBuilder('e');
      return $qb
          ->where('e.active = 1')
          ->andWhere($qb->expr()->isNull('e.approved'))
          ->andWhere($qb->expr()->eq('e.popup', $qb->expr()->literal(true)))
          ->andWhere($qb->expr()->between(
            $qb->expr()->concat('e.startDate',
              $qb->expr()->concat(
                $qb->expr()->literal(' '), 'e.startTime')
            ),
            ':now',
            ':reminderTime'
          ))
          ->setParameter('reminderTime', new \DateTime('+6 hours'), \Doctrine\DBAL\Types\Type::DATETIME)
          ->setParameter('now', $now)
          ->getQuery()
          ->execute();
    }
}
