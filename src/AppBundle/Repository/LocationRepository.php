<?php
namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class LocationRepository extends EntityRepository
{
    public function findAll() {
        $qb = $this->createQueryBuilder('l');
        return $qb
            ->orderBy('l.name')
            ->getQuery()
            ->execute();
    }
}
