<?php
namespace AppBundle\Repository;

use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository implements UserLoaderInterface
{
    public function loadUserByUsername($username)
    {
        $qb = $this->createQueryBuilder('u');
        return $qb
            ->where('u.username = :username')
            ->andWhere('u.active = 1')
            ->setParameter('username', $username)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
