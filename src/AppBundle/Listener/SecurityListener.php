<?php
namespace AppBundle\Listener;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class SecurityListener
{
    /**
     * Constructor
     *
     * @param SecurityContext $securityContext
     * @param Doctrine        $doctrine
     */
    public function __construct(AuthorizationChecker $securityContext, Session $session)
    {
        $this->securityContext = $securityContext;
        $this->session = $session;
    }

    public function onAuthenticationSuccess(InteractiveLoginEvent $event)
    {
      if ($this->securityContext->isGranted('ROLE_SUPPORT')) {
          $this->session->set('technical', true);
      }
    }
}
