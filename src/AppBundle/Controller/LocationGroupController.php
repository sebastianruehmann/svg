<?php

namespace AppBundle\Controller;

use AppBundle\Entity\LocationGroup;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Locationgroup controller.
 *
 */
class LocationGroupController extends Controller
{
    /**
     * Lists all locationGroup entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $locationGroups = $em->getRepository('AppBundle:LocationGroup')->findAll();

        return $this->render('locationgroup/index.html.twig', array(
            'locationGroups' => $locationGroups,
        ));
    }

    /**
     * Creates a new locationGroup entity.
     *
     */
    public function newAction(Request $request)
    {
        $locationGroup = new Locationgroup();
        $form = $this->createForm('AppBundle\Form\LocationGroupType', $locationGroup);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($locationGroup);
            $em->flush();

            return $this->redirectToRoute('manage_locationgroup_index');
        }

        return $this->render('locationgroup/new.html.twig', array(
            'locationGroup' => $locationGroup,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing locationGroup entity.
     *
     */
    public function editAction(Request $request, LocationGroup $locationGroup)
    {
        $deleteForm = $this->createDeleteForm($locationGroup);
        $editForm = $this->createForm('AppBundle\Form\LocationGroupType', $locationGroup);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('manage_locationgroup_index');
        }

        return $this->render('locationgroup/edit.html.twig', array(
            'locationGroup' => $locationGroup,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a locationGroup entity.
     *
     */
    public function deleteAction(Request $request, LocationGroup $locationGroup)
    {
        $form = $this->createDeleteForm($locationGroup);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($locationGroup);
            $em->flush();
        }

        return $this->redirectToRoute('manage_locationgroup_index');
    }

    /**
     * Creates a form to delete a locationGroup entity.
     *
     * @param LocationGroup $locationGroup The locationGroup entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(LocationGroup $locationGroup)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('manage_locationgroup_delete', array('id' => $locationGroup->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
