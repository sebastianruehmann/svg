<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Comment;
use AppBundle\Entity\Event;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Action controller.
 *
 */
class CommentController extends Controller
{
    public function newAction(Request $request, $technical = false, Event $event = null) {
        $comment = new Comment();
        $comment_form = $this->createForm('AppBundle\Form\CommentType', $comment);
        $comment_form->handleRequest($request);

        if ($comment_form->isSubmitted() && $comment_form->isValid()) {
            $comment->setSender($this->getUser());
            $comment->setTechnical($technical);
            $comment->setCreationTime(new \DateTime());
            $event->addComment($comment);
            $em = $this->getDoctrine()->getManager();
            $em->persist($comment);
            $em->persist($event);
            $em->flush();
        }

        return $this->render('event/comment.html.twig', array(
            'form' => $comment_form->createView(),
        ));
    }
}
