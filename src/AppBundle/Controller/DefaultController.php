<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Service\Calendar;
use AppBundle\Entity\Comment;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class DefaultController extends Controller
{
    /**
     * @Route("/calendar", name="calendar")
     */
    public function indexAction(Request $request, Calendar $calendar)
    {
        $year = $request->query->get('year') ?: date('Y');
        $month = $request->query->get('month') ?: date('m');
        $dog = $this->get('security.authorization_checker');
        $onlyTechnical = $this->get('session')->get('technical');

        $calendar = new Calendar(array(
          'url'           => '?year=%y&month=%m',
          'current_month' => $month,
          'current_year'  => $year
        ));

        $em = $this->getDoctrine()->getManager();

        if($onlyTechnical) {
            $events = $em->getRepository('AppBundle:Event')->findAllTechnical();
        } else {
            $events = $em->getRepository('AppBundle:Event')->findAllActive();
        }

        foreach ($events as $key => $event) {
            if($event->getType() == "info") {
                $calendar->add_event($event->getStartDate()->getTimeStamp(), $event->getEndDate()->getTimeStamp(), $event->getName());
            }
            else if($event->getType() == "infoaction") {
                $calendar->add_event($event->getStartDate()->getTimeStamp(), $event->getStartDate()->getTimeStamp(), $event->getName());
                $calendar->add_event($event->getEndDate()->getTimeStamp(), $event->getEndDate()->getTimeStamp(), $event->getName());
            }
            else if($event->getType() == "recurring") {
                $currentDate = $event->getStartDate()->sub($event->getInterval());
                while($currentDate->add($event->getInterval()) <= $event->getEndDate()) {
                    $calendar->add_event($currentDate->getTimeStamp(), $currentDate->getTimeStamp(), $event->getName());
                }
            }
        }

        $calendar->setup();

        return $this->render('default/index.html.twig', [
            'calendar' => $calendar,
            'onlyTechnical' => $onlyTechnical
        ]);
    }

    /**
     * @Route("/calendar/toggleTechnical", name="toggle_technical")
     */
    public function ToggleTechnicalAction() {
        if(!$this->get('session')->get('technical')) {
          $this->get('session')->set('technical', true);
        } else {
          $this->get('session')->set('technical', false);
        }

        return $this->redirectToRoute('calendar');
    }

    public function popupAction() {
        $em = $this->getDoctrine()->getManager();

        $popupRelatedEvents = $em->getRepository('AppBundle:Event')->needsPopup();

        return $this->render('default/popup.html.twig', [
            'events' => $popupRelatedEvents
        ]);
    }

    public function adminPopupAction() {
        $em = $this->getDoctrine()->getManager();
        $lt = $this->get('security.authorization_checker')->isGranted('ROLE_ADMIN') || true;

        $popupRelatedEvents = array();
        if($lt) {
          $popupRelatedEvents = $em->getRepository('AppBundle:Event')->needsNotActivePopup();
        }

        return $this->render('default/adminPopup.html.twig', [
            'events' => $popupRelatedEvents
        ]);
    }

    /**
     * @Route("/calendar/daily/{dateTime}", name="show_daily_events")
     */
    public function listAction($dateTime)
    {
        try {
            $date = new \DateTime($dateTime);
        } catch (Exception $e) {
            echo $e->getMessage();
            exit(1);
        }
        $em = $this->getDoctrine()->getManager();

        if($this->get('session')->get('technical')) {
          $events = $em->getRepository('AppBundle:Event')->findAllDaily($date, true);
        } else {
          $events = $em->getRepository('AppBundle:Event')->findAllDaily($date);
        }


        return $this->render('default/list.html.twig', [
            'events' => $events,
            'date' => $date
        ]);
    }

    /**
     * @Route("/calendar/event/{id}", name="show_event")
     */
    public function showAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $event = $em->getRepository('AppBundle:Event')->findOneById($id);

        $inspect = $this->get('form.factory')->createNamed('inspect','AppBundle\Form\InspectionType', $event);
        $inspect->handleRequest($request);

        if ($inspect->isSubmitted() && $inspect->isValid()) {
            if ($inspect->get('inspected')->isClicked()) {
              $event->setInspectionDate(new \DateTime());
              $event->setInspector($this->getUser());
            }
            if ($inspect->get('uninspect')->isClicked()) {
              $event->setInspectionDate(null);
              $event->setInspector(null);
            }

            $em->persist($event);
            $em->flush();

            return $this->redirectToRoute('show_event', array('id' => $event->getId()));
        }

        $second_inspect = $this->get('form.factory')->createNamed('second_inspect','AppBundle\Form\InspectionType', $event);
        $second_inspect->handleRequest($request);

        if ($second_inspect->isSubmitted() && $second_inspect->isValid()) {
            if ($second_inspect->get('inspected')->isClicked()) {
              $event->setSecondInspectionDate(new \DateTime());
              $event->setSecondInspector($this->getUser());
            }
            if ($second_inspect->get('uninspect')->isClicked()) {
              $event->setSecondInspectionDate(null);
              $event->setSecondInspector(null);
            }

            $em->persist($event);
            $em->flush();

            return $this->redirectToRoute('show_event', array('id' => $event->getId()));
        }

        $tech_inspect = $this->get('form.factory')->createNamed('tech_inspect','AppBundle\Form\InspectionType', $event);
        $tech_inspect->handleRequest($request);

        if ($tech_inspect->isSubmitted() && $tech_inspect->isValid()) {
            if ($tech_inspect->get('inspected')->isClicked()) {
              $event->setTechnicalInspectionDate(new \DateTime());
              $event->setTechnicalInspector($this->getUser());
            }
            if ($tech_inspect->get('uninspect')->isClicked()) {
              $event->setTechnicalInspectionDate(null);
              $event->setTechnicalInspector(null);
            }

            $em->persist($event);
            $em->flush();

            return $this->redirectToRoute('show_event', array('id' => $event->getId()));
        }

        $second_tech_inspect = $this->get('form.factory')->createNamed('second_tech_inspect','AppBundle\Form\InspectionType', $event);
        $second_tech_inspect->handleRequest($request);

        if ($second_tech_inspect->isSubmitted() && $second_tech_inspect->isValid()) {
            if ($second_tech_inspect->get('inspected')->isClicked()) {
              $event->setSecondTechnicalInspectionDate(new \DateTime());
              $event->setSecondTechnicalInspector($this->getUser());
            }
            if ($second_tech_inspect->get('uninspect')->isClicked()) {
              $event->setSecondTechnicalInspectionDate(null);
              $event->setSecondTechnicalInspector(null);
            }

            $em->persist($event);
            $em->flush();

            return $this->redirectToRoute('show_event', array('id' => $event->getId()));
        }

        $comment = new Comment();
        $comment_form = $this->get('form.factory')->createNamedBuilder('comment_form', 'Symfony\Component\Form\Extension\Core\Type\FormType', $comment)
           ->add('message')
           ->add('save', SubmitType::class, array('label' => 'Abschicken'))
           ->getForm();
        $comment_form->handleRequest($request);

        if ($comment_form->isSubmitted() && $comment_form->isValid()) {
            $comment->setSender($this->getUser());
            $comment->setTechnical(false);
            $comment->setCreationTime(new \DateTime());
            $event->addComment($comment);
            $em = $this->getDoctrine()->getManager();
            $em->persist($comment);
            $em->persist($event);
            $em->flush();

            return $this->redirectToRoute('show_event', array('id' => $event->getId()));
        }

        $tech_comment = new Comment();
        $tech_comment_form = $this->get('form.factory')->createNamedBuilder('tech_comment_form', 'Symfony\Component\Form\Extension\Core\Type\FormType', $tech_comment)
           ->add('message')
           ->add('save', SubmitType::class, array('label' => 'Abschicken'))
           ->getForm();
        $tech_comment_form->handleRequest($request);

        if ($tech_comment_form->isSubmitted() && $tech_comment_form->isValid()) {
            $tech_comment->setSender($this->getUser());
            $tech_comment->setTechnical(true);
            $tech_comment->setCreationTime(new \DateTime());
            $event->addComment($tech_comment);
            $em = $this->getDoctrine()->getManager();
            $em->persist($comment);
            $em->persist($event);
            $em->flush();

            return $this->redirectToRoute('show_event', array('id' => $event->getId()));
        }

        $approved_form = $this->get('form.factory')->createNamedBuilder('approved_form', 'Symfony\Component\Form\Extension\Core\Type\FormType')
           ->add('approved', SubmitType::class, array('label' => 'Freigeben'))
           ->getForm();

        $approved_form->handleRequest($request);

        if ($approved_form->isSubmitted() && $approved_form->isValid()) {
            if ($approved_form->get('approved')->isClicked()) {
               $event->setApproved(true);

               $em->persist($event);
               $em->flush();

               return $this->redirectToRoute('show_event', array('id' => $event->getId()));
           }
        }

        return $this->render('event/show.html.twig', [
            'event' => $event,
            'tech_inspect_form' => $tech_inspect->createView(),
            'inspect_form' => $inspect->createView(),
            'second_tech_inspect_form' => $second_tech_inspect->createView(),
            'second_inspect_form' => $second_inspect->createView(),
            'comment_form' => $comment_form->createView(),
            'tech_comment_form' => $tech_comment_form->createView(),
            'approved_form' => $approved_form->createView()
        ]);
    }
}
