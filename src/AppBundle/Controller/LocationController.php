<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Location;
use AppBundle\Entity\Document;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Service\DocumentUploader;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Location controller.
 *
 */
class LocationController extends Controller
{
    /**
     * Lists all location entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $locations = $em->getRepository('AppBundle:Location')->findAll();
        $locationgroups = $em->getRepository('AppBundle:LocationGroup')->findAll();

        return $this->render('location/index.html.twig', array(
            'groups' => $locationgroups,
            'locations' => $locations
        ));
    }

    /**
     * Creates a new location entity.
     *
     */
    public function newAction(Request $request, DocumentUploader $documentUploader)
    {
        $location = new Location();

        $document = new Document();
        $location->addDocument($document);
        $form = $this->createForm('AppBundle\Form\LocationType', $location, array(
            'upload_path' => $this->getParameter('upload_path'),
        ));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $documents = $location->getDocuments();
            $em = $this->getDoctrine()->getManager();

            if (null !== $documents[0]->getFile()) {
                foreach($documents as $document) {
                    $file = $document->getFile();
                    $filePath = $documentUploader->upload($file);
                    $document->setFile($filePath);

                    $em->persist($document);
                }
            } else {
              $location->removeDocument($document);
            }

            $em->persist($location);
            $em->flush();

            return $this->redirectToRoute('manage_location_index');
        }

        return $this->render('location/new.html.twig', array(
            'location' => $location,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a location entity.
     *
     */
    public function showAction(Location $location)
    {
        $deleteForm = $this->createDeleteForm($location);

        return $this->render('location/show.html.twig', array(
            'location' => $location,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing location entity.
     *
     */
    public function editAction(Request $request, Location $location, DocumentUploader $documentUploader)
    {
        $originalDocuments = new ArrayCollection();

        foreach ($location->getDocuments() as $document) {
            $originalDocuments->add($document);
        }

        $deleteForm = $this->createDeleteForm($location);

        $editForm = $this->createForm('AppBundle\Form\LocationType', $location, array(
            'upload_path' => $this->getParameter('upload_path'),
        ));
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $documents = $location->getDocuments();
            $em = $this->getDoctrine()->getManager();

            foreach($documents as $document) {
                if(!$originalDocuments->contains($document)) {
                    $file = $document->getFile();
                    $filePath = $documentUploader->upload($file);
                    $document->setFile($filePath);
                }
                $em->persist($document);
            }

            $em->persist($location);
            $em->flush();

            return $this->redirectToRoute('manage_location_index');
        }

        return $this->render('location/edit.html.twig', array(
            'location' => $location,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a location entity.
     *
     */
    public function deleteAction(Request $request, Location $location)
    {
        $form = $this->createDeleteForm($location);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($location);
            $em->flush();
        }

        return $this->redirectToRoute('manage_location_index');
    }

    /**
     * Creates a form to delete a location entity.
     *
     * @param Location $location The location entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Location $location)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('manage_location_delete', array('id' => $location->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
