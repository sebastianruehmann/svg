<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Event;
use AppBundle\Entity\Sender;
use AppBundle\Entity\Document;
use AppBundle\Entity\Location;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Service\DocumentUploader;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Event controller.
 *
 */
class EventController extends Controller
{
    /**
     * Lists all event entities.
     *
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $state = $request->query->get('event_filter')['state'];
        $locationId = $request->query->get('event_filter')['location'];

        $location = '';
        if(!empty($locationId)) {
          $location = $this->getDoctrine()->getEntityManager()->getRepository('AppBundle:Location')->find($locationId);
        }

        $events = $em->getRepository('AppBundle:Event')->findAllFiltered($location, $state);

        $filter_form = $this->get('form.factory')->createNamedBuilder('event_filter', 'AppBundle\Form\EventFilterType', null, array(
            'state' => $state,
            'location' => $location
        ))
            ->setMethod('GET')
            ->getForm();

        return $this->render('event/index.html.twig', array(
            'events' => $events,
            'filter_form' => $filter_form->createView()
        ));
    }

    /**
     * Creates a new event entity.
     *
     */
    public function newAction(Request $request, DocumentUploader $documentUploader)
    {
        $event = new Event();
        $sender = new Sender();
        $document = new Document();
        $event->setSender($sender);
        $event->addDocument($document);

        $form = $this->createForm('AppBundle\Form\EventType', $event, array(
            'upload_path' => $this->getParameter('upload_path'),
        ));

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $i = $event->getInterval();
            if($event->getType() == 'recurring' && $i->y == 0 && $i->m == 0 && $i->d == 0) {
                $error = new \Symfony\Component\Form\FormError("Bitte geben Sie bei einem wiederkehrenden Event einen Interval an", null);
                $form->get('interval')->addError($error);
            }

            if($form->isValid()) {
                $documents = $event->getDocuments();
                $em = $this->getDoctrine()->getManager();

                $event->setCreationDate(new \DateTime());
                $event->setCreator($this->getUser());

                if (null !== $documents[0]->getFile()) {
                    foreach($documents as $document) {
                        $file = $document->getFile();
                        $filePath = $documentUploader->upload($file);
                        $document->setFile($filePath);

                        $em->persist($document);
                    }
                } else {
                  $event->removeDocument($document);
                }

                $em->persist($sender);
                $em->persist($event);
                $em->flush();

                return $this->redirectToRoute('manage_event_index');
            }
        }

        return $this->render('event/new.html.twig', array(
            'event' => $event,
            'form' => $form->createView(),
        ));
    }

    /**
     * Duplicate a event entity.
     *
     */
    public function duplicateAction(Request $request, Event $event)
    {
        $form = $this->createDuplicateForm($event);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $new_event = clone $event;
            $em = $this->getDoctrine()->getManager();
            $em->persist($new_event);
            $em->flush();

            return $this->redirectToRoute('manage_event_edit', array('id' => $new_event->getId()));
        }
    }

    /**
     * Creates a form to duplicate a event entity.
     *
     * @param Event $event The event entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDuplicateForm(Event $event)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('manage_event_duplicate', array('id' => $event->getId())))
            ->getForm()
        ;
    }

    /**
     * Displays a form to edit an existing event entity.
     *
     */
    public function editAction(Request $request, Event $event, DocumentUploader $documentUploader)
    {
        $originalDocuments = new ArrayCollection();

        foreach ($event->getDocuments() as $document) {
            $originalDocuments->add($document);
        }

        $duplicateForm = $this->createDuplicateForm($event);
        $deleteForm = $this->createDeleteForm($event);

        $editForm = $this->createForm('AppBundle\Form\EventType', $event, array(
            'upload_path' => $this->getParameter('upload_path'),
        ));
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted()) {
            $i = $event->getInterval();
            if($event->getType() == 'recurring' && $i->y == 0 && $i->m == 0 && $i->d == 0) {
              $error = new \Symfony\Component\Form\FormError("Bitte geben Sie bei einem wiederkehrenden Event einen Interval an", null);
              $editForm->get('interval')->addError($error);
            }

            if($editForm->isValid()) {
                $documents = $event->getDocuments();
                $em = $this->getDoctrine()->getManager();

                foreach($documents as $document) {
                    if(!$originalDocuments->contains($document)) {
                        $file = $document->getFile();

                        $filePath = $documentUploader->upload($file);
                        $document->setFile($filePath);
                    }
                    $em->persist($document);
                }

                $em->persist($event);
                $em->flush();

                return $this->redirectToRoute('manage_event_index');
            }
        }

        return $this->render('event/edit.html.twig', array(
            'event' => $event,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'duplicate_form' => $duplicateForm->createView(),
        ));
    }

    /**
     * Deletes a event entity.
     *
     */
    public function deleteAction(Request $request, Event $event)
    {
        $form = $this->createDeleteForm($event);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($event);
            $em->flush();
        }

        return $this->redirectToRoute('manage_event_index');
    }

    /**
     * Creates a form to delete a event entity.
     *
     * @param Event $event The event entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Event $event)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('manage_event_delete', array('id' => $event->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    public function searchAction(Request $request)
    {
        $q = $request->query->get('term');
        $results = $this->getDoctrine()->getRepository('AppBundle:Event')->findLikeName($q);

        return $this->render('event/name_autocomplete.html.twig', ['results' => $results]);
    }

    public function getAction($name = null)
    {
        return new Response($name);
    }
}
