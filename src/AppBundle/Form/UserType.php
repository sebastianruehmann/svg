<?php

namespace AppBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class UserType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('active')->add('name')->add('username')->add('telephone')->add('plainPassword', PasswordType::class, array(
          'required' => true
        ));
        $builder->add('groups', EntityType::class, array(
            'class' => 'AppBundle:Group',
            'choice_label' => 'description',
            'choice_value' => 'name',
            'multiple' => true,
            'expanded' => true
        ));
        $builder->add('department', ChoiceType::class, array(
            'choices'  => array(
                'Leitstelle'=>'Leitstelle',
                'Techniker'=>'Techniker',
                'Azubi'=>'Azubi'
            ),
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_user';
    }


}
