<?php

namespace AppBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class EventFilterType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add('location', EntityType::class, array(
              'class' => 'AppBundle:Location',
              'required' => false,
              'data' => $options['location'],
              'choice_label' => 'name'
          ))
          ->add('state', ChoiceType::class, array(
              'label' => 'Zustand',
              'required' => false,
              'data' => $options['state'],
              'choices'  => array(
                  'Veröffentlicht' => '1',
                  'Unveröffentlicht' => '0'
              )
          ))
          ->add('save', SubmitType::class, array('label' => 'Filtern'));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
          'location' => '',
          'state' => ''
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'event_filter';
    }


}
