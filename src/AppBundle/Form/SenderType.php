<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class SenderType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name')->add('date', DateType::class, array(
            'attr' => array(
                'class' => 'datepicker'
            ),
            'widget' => 'single_text',
            'format' => 'dd.MM.yyyy',
            'required' => false
        ));
        $builder->add('status', ChoiceType::class, array(
            'choices'  => array(
                'Event wurde ordnungsgemäß gemeldet'=>'InTime',
                'Event wurde zu spät gemeldet'=>'Delayed',
                'Event wurde gar nicht gemeldet'=>'Failed'
            ),
            'expanded' => true
        ));
        $builder->add('medium', ChoiceType::class, array(
            'choices'  => array(
                'E-Mail'=>'E-Mail',
                'Telefon'=>'Telefon',
                'Fax'=>'Fax'
            ),
            'expanded' => true
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Sender'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_sender';
    }


}
