<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DocumentType extends AbstractType
{

    protected $upload_path;

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
      $this->upload_path = $options['upload_path'];

      $builder->add(
          $builder->create('file', FileType::class, array(
            'label' => 'Dokument'
          ))
              ->addModelTransformer(new CallbackTransformer(
                function ($filename) {
                    if (!$filename) {
                        return null;
                    }
                    return new \Symfony\Component\HttpFoundation\File\File($this->upload_path .'/'. $filename, false);
                },
                function ($file) {
                    return $file;
                }
              ))
      );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Document',
            'upload_path' => ''
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_document';
    }


}
