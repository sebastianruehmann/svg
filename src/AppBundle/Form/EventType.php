<?php

namespace AppBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\DateIntervalType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class EventType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add('name')
          ->add('active', CheckboxType::class, array(
            'data' => true,
            'required' => false
          ))
          ->add('important')
          ->add('popup')
          ->add('specials')
          ->add('description')
          ->add('regularPrice', CheckboxType::class, array(
            'data' => true,
            'required' => false
          ))
          ->add('technicalDescription')
          ->add('type', ChoiceType::class, array(
              'choices'  => array(
                  'Wiederkehrendes Event'=>'recurring',
                  'Info'=>'info',
                  'Info Start / Stopp'=>'infoaction'
              )
          ))
          ->add('startTime', TimeType::class, array(
              'attr' => array(
                  'class' => 'timepicker'
              ),
              'widget' => 'single_text',
          ))
          ->add('endTime', TimeType::class, array(
              'attr' => array(
                  'class' => 'timepicker'
              ),
              'widget' => 'single_text',
          ))
          ->add('startDate', DateType::class, array(
              'attr' => array(
                  'class' => 'datepicker'
              ),
              'format' => 'dd.MM.yyyy',
              'widget' => 'single_text',
          ))
          ->add('endDate', DateType::class, array(
              'attr' => array(
                  'class' => 'datepicker'
              ),
              'format' => 'dd.MM.yyyy',
              'widget' => 'single_text',
          ))
          ->add('secondAction')
          ->add('secondTechnicalAction')
          ->add('interval', DateIntervalType::class, array(
              'widget'      => 'integer', // render a text field for each part

              // customize which text boxes are shown
              'with_years'  => true,
              'with_months' => true,
              'with_days'   => true,
              'with_hours'  => false,
              'required' => false,
              'labels' => array(
                  'invert' => null,
                  'years' => 'Jahre',
                  'months' => 'Monate',
                  'days' => 'Tage',
                  'hours' => null,
                  'minutes' => null,
                  'seconds' => null,
              )
          ))
          ->add('location', EntityType::class, array(
              'class' => 'AppBundle:Location',
              'choice_label' => 'name'
          ))
          ->add('altAction')
          ->add('action', EntityType::class, array(
              'class' => 'AppBundle:Action',
              'choice_label' => 'name',
              'required' => false,
              'query_builder' => function (EntityRepository $er) {
                  return $er->createQueryBuilder('a')
                      ->where('a.technical = false');
              },
          ))
          ->add('technicalAction', EntityType::class, array(
              'class' => 'AppBundle:Action',
              'choice_label' => 'name',
              'query_builder' => function (EntityRepository $er) {
                  return $er->createQueryBuilder('a')
                      ->where('a.technical = true');
              },
              'required' => false
          ))
          ->add('sender', SenderType::class, array(
            'required' => false
          ))
          ->add('documents', CollectionType::class,array(
              'required' => false,
              'entry_type' => DocumentType::class,
              'allow_add' => true,
              //'allow_delete' => true,
              'prototype' => true,
              'by_reference' => false,
              'entry_options' => array(
                'upload_path' => $options['upload_path']
              )
          ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Event',
            'upload_path' => ''
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_event';
    }


}
