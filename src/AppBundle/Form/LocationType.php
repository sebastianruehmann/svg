<?php

namespace AppBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class LocationType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name')->add('street')->add('houseNumber')->add('zip')->add('state')->add('dialingCode')->add('phoneNumber')->add('email')->add('contactPerson')->add('description');

        $builder->add('group', EntityType::class, array(
            'class' => 'AppBundle:LocationGroup',
            'choice_label' => 'name',
            'required' => false
        ));
        $builder->add('documents', CollectionType::class,array(
            'required' => false,
            'entry_type' => DocumentType::class,
            'allow_add' => true,
            //'allow_delete' => true,
            'prototype' => true,
            'by_reference' => false,
            'entry_options' => array(
              'upload_path' => $options['upload_path']
            )
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Location',
            'upload_path' => ''
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_location';
    }


}
